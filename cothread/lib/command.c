﻿/* 简介：cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。
 * 同时，由于该调度器仅仅运行在一个实际线程中，所以它也适用于服务器高并发场景。
 *
 * 版本: 1.0.0   2019/02/25
 *
 * 作者: 覃攀 <qinpan1003@qq.com>
 *
 */

#include "rtos.h"

/*------------------------------------------------------------------------------------*/
// 命令
static int help(int argc,char **argv)
{
    int i;

    for(i = 0; i < command_nr; i++)
        LOG("%s\n", command_table[i].name);
    
    return  0;
}
/*------------------------------------------------------------------------------------*/
static int md(int argc,char **argv)
{
#define USAGE   "usage:md addr len\n"
    unsigned char *addr;
    int len;
    char i;
    
    if(argc != 3)
    {
        LOG(USAGE);
        return -1;
    }
    
    addr = (unsigned char *)(unsigned long)atoi(argv[1]);
    len = atoi(argv[2]);
    
    LOG("addr = %p,len = 0x%x :\n", addr, len);
    i = 0;
    while(len != 0)
    {        
        if(i == 0)
            LOG("%p: ", addr);
        
        if(i == 8)
            LOG("  ");
        
        LOG("%x ",(*addr) & 0xff);
        addr++;
        len--;
        
        if(++i == 16)
        {
            i = 0;
            LOG("\n");
        }
    }
    return  0;
}
/*------------------------------------------------------------------------------------*/
static int ascii(int argc,char **argv)
{
    int c, i, j;
    
    LOG("    0 1 2 3 4 5 6 7    8 9 A B C D E F");

    /* 打印 0x20 - 0x7f 的字符,都是可显示字符 */
    for (c = 0x20,i = 0,j = 2;c < 0x80;c++,i++)
    {
        if(i == 16)
        {
            i = 0;
            j++;
        }
        
        if(i == 0)
            LOG("\n%x : ",(int)j);
        
        if(i == 8)
            LOG("   ");

        LOG("%c ",c);
    }
    LOG("\n");
    return  0;
}
/*------------------------------------------------------------------------------------*/
int main(void);
static int reboot(int argc,char **argv)
{
    int s = 5;

    LOG("system going to reboot...  ");
    while(s > 0)
    {
        LOG("\b\b %d",(int)(--s));
        //os_wait2(K_TMO,100);
        //__delay_cycles(12000000);
    }

    main();

    return  0;
}

static int show(int argc,char **argv)
{
    os_show_stat();
    return 0;
}

int thread_test(int argc,char **argv);
int enable_irq_log(int argc,char **argv);

/*------------------------------------------------------------------------------------*/
// 命令表
const command_t command_table[] =
{
    {"help",    help},
    {"md",      md},
    {"ascii",   ascii},
    {"reboot",  reboot},
    {"thread_test", thread_test},
    {"show", show},
    {"irq_log", enable_irq_log},
};

const int command_nr = sizeof(command_table) / sizeof(command_table[0]);
/*------------------------------------------------------------------------------------*/

